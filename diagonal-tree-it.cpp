vector<int> Solution::solve(TreeNode* A) {
    vector<int> ans;
    if(!A) return ans;
    deque<TreeNode*> dq;
    dq.push_back(A);
    while(!dq.empty()) {
        TreeNode* node = dq.front();
        dq.pop_front();
        ans.push_back(node -> val);
        if(node -> right) dq.push_front(node -> right);
        if(node -> left) dq.push_back(node -> left);
    }
    return ans;
}
