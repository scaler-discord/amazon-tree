int inorder(TreeNode* root) {
    int lastNum = INT_MIN, ans = -1;
    bool found = false;
    while(root) {
        if(root -> left) {
            TreeNode* prev = NULL, *cur = root -> left;
            while(cur && cur != root) {
                prev = cur;
                cur = cur -> right;
            }
            if(!cur) {
                prev -> right = root;
                root = root -> left;
                continue;
            }
            prev -> right = NULL;
        }
        if(!found && lastNum > root -> val) {
            ans = lastNum;
            found = true;
        }
        lastNum = root -> val;
        root = root -> right;
    }
    return ans;
}

int rinorder(TreeNode* root) {
    int lastNum = INT_MAX, ans = -1;
    bool found = false;
    while(root) {
        if(root -> right) {
            TreeNode* prev = NULL, *cur = root -> right;
            while(cur && cur != root) {
                prev = cur;
                cur = cur -> left;
            }
            if(!cur) {
                prev -> left = root;
                root = root -> right;
                continue;
            }
            prev -> left = NULL;
        }
        if(!found && lastNum < root -> val) {
            ans = lastNum;
            found = true;
        }
        lastNum = root -> val;
        root = root -> left;
    }
    return ans;
}

vector<int> Solution::recoverTree(TreeNode* A) {
    return vector<int>{rinorder(A), inorder(A)};
}
