void ltr(stack<TreeNode*>& cur, stack<TreeNode*>& next, vector<vector<int>>& ans) {
    if(cur.empty()) return;
    vector<int> v;
    while(!cur.empty()) {
        TreeNode* node = cur.top(); cur.pop();
        v.push_back(node -> val);
        if(node -> left) next.push(node -> left);
        if(node -> right) next.push(node -> right);
    }
    ans.push_back(v);
}

void rtl(stack<TreeNode*>& cur, stack<TreeNode*>& next, vector<vector<int>>& ans) {
    if(cur.empty()) return;
    vector<int> v;
    while(!cur.empty()) {
        TreeNode* node = cur.top(); cur.pop();
        v.push_back(node -> val);
        if(node -> right) next.push(node -> right);
        if(node -> left) next.push(node -> left);
    }
    ans.push_back(v);
}

vector<vector<int> > Solution::zigzagLevelOrder(TreeNode* A) {
    vector<vector<int>> ans;
    if(!A) return ans;
    stack<TreeNode*> s1, s2;
    s1.push(A);
    while(!s1.empty() || !s2.empty()) {
        ltr(s1, s2, ans);
        rtl(s2, s1, ans);
    }
    return ans;
}
