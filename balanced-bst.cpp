bool isBal(TreeNode* root, int& h) {
    if(!root) {
        h = 0;
        return true;
    }
    int lh, rh;
    if(!isBal(root->left, lh)) return false;
    if(!isBal(root->right, rh)) return false;
    h = 1 + max(lh, rh);
    return abs(lh-rh) <= 1;
}

int Solution::isBalanced(TreeNode* A) {
    int h;
    return isBal(A, h);
}
