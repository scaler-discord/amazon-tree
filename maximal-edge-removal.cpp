int dfs(vector<int> adj[], int u, int p, int &ans) {
    int nodes = 1;
    for(auto& v : adj[u]) {
        if(v != p) nodes += dfs(adj, v, u, ans);
    }
    if(nodes % 2 == 0) ans++;
    return nodes;
}

int Solution::solve(int n, vector<vector<int> > &edges) {
    vector<int> adj[n];
    for(auto& edge : edges) {
        int u = edge[0] - 1, v = edge[1] - 1;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    int ans = 0;
    vector<bool> vis(n, false);
    dfs(adj, 0, -1, ans);
    return ans - 1;
}
